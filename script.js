// TODO - Remove this file after adding all hardcoded default cardpacks.
// Use at https://pyx-1.pretendyoure.xyz/zy/viewcards.jsp
// Select the cardpack you want, and then run this script in the console
// to get nicely formatted Rust code containing hardcoded data for that
// cardpack.

var items = [];
var test = document.getElementsByTagName("tbody")[0];

for (let i = 0; i < test.children.length; i++) {
  let elem = test.children[i];
  if (elem.style.display === 'table-row') {
    items.push({
      type: elem.children[0].innerText,
      text: elem.children[1].innerText,
      source: elem.children[2].innerText,
      draw: elem.children[3].innerText,
      pick: elem.children[4].innerText
    });
  }
}

var blackCards = [];
var whiteCards = [];

items.forEach((item) => {
  if (item.type === 'White') {
    whiteCards.push({
      text: item.text
    });
  } else {
    blackCards.push({
      text: item.text,
      pick: item.pick
    });
  }
});

console.log(`(\n    String::from("Hello world!"),\n    vec!{\n${blackCards.map((card) => `        (String::from(${JSON.stringify(card.text)}), ${card.pick})`).join(',\n')}\n    },\n    vec!{\n${whiteCards.map((card) => `        String::from(${JSON.stringify(card.text)})`).join(',\n')}\n    }\n)`);