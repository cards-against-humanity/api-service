pub mod custom_black_card_collection;
pub mod custom_cardpack_collection;
pub mod custom_white_card_collection;
mod helper;
pub mod user_collection;

use super::environment::EnvironmentVariables;
use mongodb::{Client, Database};

pub async fn get_mongo_database_or_panic(env_vars: &EnvironmentVariables) -> Database {
    let mongo_client = match Client::with_uri_str(env_vars.get_mongo_uri()).await {
        Ok(client) => client,
        _ => panic!("Failed to connect to MongoDB."),
    };

    mongo_client.database(&env_vars.get_mongo_database())
}
