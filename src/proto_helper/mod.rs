pub mod page_token;
pub mod resource_name;
pub mod time;

use crate::proto::{user_settings::ColorScheme, GameConfig, OAuthCredentials};
use crate::service::helper::parse_name_error;
use tonic::Status;

#[derive(Clone)]
pub struct ValidatedStringField {
    string: String,
}

impl ValidatedStringField {
    pub fn new(s: &str, field_name: &str) -> Result<Self, Status> {
        let string = String::from(s.trim());
        if string.is_empty() {
            return Err(empty_request_field_error(field_name));
        }
        Ok(Self { string })
    }

    pub fn get_string(&self) -> &str {
        &self.string
    }

    pub fn take_string(self) -> String {
        self.string
    }
}

pub struct ValidatedOAuthCredentials {
    oauth_credentials: OAuthCredentials,
}

impl ValidatedOAuthCredentials {
    pub fn new(oauth_credentials: &OAuthCredentials, field_name: &str) -> Result<Self, Status> {
        Ok(Self {
            oauth_credentials: OAuthCredentials {
                oauth_provider: ValidatedStringField::new(
                    &oauth_credentials.oauth_provider,
                    &format!("{}.oauth_provider", field_name),
                )?
                .take_string(),
                oauth_id: ValidatedStringField::new(
                    &oauth_credentials.oauth_id,
                    &format!("{}.oauth_id", field_name),
                )?
                .take_string(),
            },
        })
    }

    pub fn take_oauth_credentials(self) -> OAuthCredentials {
        self.oauth_credentials
    }
}

pub struct ValidatedGameConfig {
    game_config: GameConfig,
}

impl ValidatedGameConfig {
    pub fn new(game_config: &GameConfig, field_name: &str) -> Result<Self, Status> {
        let mut custom_cardpack_names = Vec::new();
        for name in game_config.custom_cardpack_names.iter() {
            let validated_name = match resource_name::CustomCardpackName::new_from_str(&name) {
                Ok(name) => Ok(name),
                Err(err) => Err(parse_name_error(err)),
            }?
            .clone_str();
            custom_cardpack_names.push(validated_name);
        }

        let mut default_cardpack_names = Vec::new();
        for name in game_config.default_cardpack_names.iter() {
            let validated_name = match resource_name::DefaultCardpackName::new_from_str(&name) {
                Ok(name) => Ok(name),
                Err(err) => Err(parse_name_error(err)),
            }?
            .clone_str();
            default_cardpack_names.push(validated_name);
        }

        Ok(Self {
            game_config: GameConfig {
                display_name: ValidatedStringField::new(
                    &game_config.display_name,
                    &format!("{}.display_name", field_name),
                )?
                .take_string(),
                // TODO - Replace `min` (2) and `max` (100) with MIN_PLAYER_LIMIT and MAX_PLAYER_LIMIT constants that belong to the game service.
                max_players: BoundedNumberField::new(
                    game_config.max_players,
                    2,
                    100,
                    &format!("{}.max_players", field_name),
                )?
                .take_value(),
                // TODO - Replace `min` (3) and `max` (20) with MIN_HAND_SIZE_LIMIT and MAX_HAND_SIZE_LIMIT constants that belong to the game service.
                hand_size: BoundedNumberField::new(
                    game_config.max_players,
                    3,
                    20,
                    &format!("{}.hand_size", field_name),
                )?
                .take_value(),
                custom_cardpack_names,
                default_cardpack_names,
                // TODO - Validate blank_white_card_config.
                blank_white_card_config: game_config.blank_white_card_config.clone(),
                // TODO - Validate end_condition.
                end_condition: game_config.end_condition.clone(),
            },
        })
    }

    pub fn take_game_config(self) -> GameConfig {
        self.game_config
    }
}

pub struct BoundedNumberField {
    value: i32,
}

impl BoundedNumberField {
    pub fn new(value: i32, min: i32, max: i32, field_name: &str) -> Result<Self, Status> {
        if min > max {
            return Err(Status::internal(
                "Min cannot not be greater than max when instantiating BoundedNumberField.",
            ));
        }

        if value < min || value > max {
            return Err(Status::invalid_argument(format!(
                "Request field `{}` must be between {} and {} (inclusive).",
                field_name, min, max
            )));
        }

        Ok(Self { value })
    }

    pub fn get_value(&self) -> &i32 {
        &self.value
    }

    pub fn take_value(self) -> i32 {
        self.value
    }
}

pub struct AnswerFieldCount {
    count: BoundedNumberField,
}

impl AnswerFieldCount {
    pub fn new(count: i32, field_name: &str) -> Result<Self, Status> {
        if count == 0 {
            return Err(empty_request_field_error(field_name));
        }

        Ok(Self {
            count: BoundedNumberField::new(count, 1, 3, field_name)?,
        })
    }

    pub fn get_count(&self) -> &i32 {
        &self.count.get_value()
    }

    pub fn take_count(self) -> i32 {
        self.count.take_value()
    }
}

pub struct BoundedPageSize {
    page_size: i64,
}

impl BoundedPageSize {
    fn get_bounded_page_size(page_size: i32) -> Result<i64, Status> {
        if page_size < 0 {
            return Err(Status::invalid_argument("Page size cannot be negative."));
        } else if page_size == 0 {
            return Ok(50);
        } else if page_size > 1000 {
            return Ok(1000);
        } else {
            return Ok(page_size as i64);
        }
    }

    pub fn new(page_size: i32) -> Result<Self, Status> {
        Ok(Self {
            page_size: Self::get_bounded_page_size(page_size)?,
        })
    }

    // This returns an i64 because that's what mongodb takes as the query return limit.
    pub fn take_i64(self) -> i64 {
        self.page_size
    }
}

pub enum ValidatedColorScheme {
    DefaultLight = 1,
    DefaultDark = 2,
}

impl ValidatedColorScheme {
    pub fn new(color_scheme_i32: i32) -> Result<Self, Status> {
        let color_scheme = match ColorScheme::from_i32(color_scheme_i32) {
            Some(color_scheme) => color_scheme,
            None => {
                return Err(Status::invalid_argument(
                    "Color scheme must be a currently supported value.",
                ))
            }
        };

        Ok(match color_scheme {
            ColorScheme::Unspecified => {
                return Err(Status::invalid_argument(
                    "Color scheme cannot be unspecified.",
                ))
            }
            ColorScheme::DefaultLight => ValidatedColorScheme::DefaultLight,
            ColorScheme::DefaultDark => ValidatedColorScheme::DefaultDark,
        })
    }
}

pub enum OptionalField<T> {
    Set(T),
    Unset,
}

fn empty_request_field_error(field_name: &str) -> Status {
    Status::invalid_argument(format!("Request field `{}` must not be blank.", field_name))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_bounded_page_size() {
        assert_eq!(format!("{}", BoundedPageSize::get_bounded_page_size(-1000).unwrap_err()), "status: InvalidArgument, message: \"Page size cannot be negative.\", details: [], metadata: MetadataMap { headers: {} }");
        assert_eq!(format!("{}", BoundedPageSize::get_bounded_page_size(-1).unwrap_err()), "status: InvalidArgument, message: \"Page size cannot be negative.\", details: [], metadata: MetadataMap { headers: {} }");
        assert_eq!(BoundedPageSize::get_bounded_page_size(0).unwrap(), 50);
        assert_eq!(BoundedPageSize::get_bounded_page_size(1).unwrap(), 1);
        assert_eq!(BoundedPageSize::get_bounded_page_size(100).unwrap(), 100);
        assert_eq!(BoundedPageSize::get_bounded_page_size(1000).unwrap(), 1000);
        assert_eq!(BoundedPageSize::get_bounded_page_size(1001).unwrap(), 1000);
        assert_eq!(BoundedPageSize::get_bounded_page_size(10000).unwrap(), 1000);
    }
}
