use bson::oid::ObjectId;
use prost_types::Timestamp;

pub fn object_id_to_timestamp_proto(object_id: &ObjectId) -> Timestamp {
    chrono_timestamp_to_timestamp_proto(&object_id.timestamp())
}

pub fn chrono_timestamp_to_timestamp_proto(
    chrono_timestamp: &chrono::DateTime<chrono::offset::Utc>,
) -> Timestamp {
    let mut seconds = chrono_timestamp.timestamp();
    let mut nanos = chrono_timestamp.timestamp_subsec_nanos();

    if nanos > 999999999 {
        nanos -= 1000000000;
        seconds += 1;
    }

    Timestamp {
        seconds,
        nanos: nanos as i32,
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use chrono::{DateTime, NaiveDateTime, Utc};

    #[test]
    fn test_object_id_to_timestamp_proto() {
        let object_id = ObjectId::with_string("507c7f79bcf86cd7994f6c0e").unwrap();
        let timestamp = object_id_to_timestamp_proto(&object_id);
        assert_eq!(
            timestamp,
            Timestamp {
                seconds: 1350336377,
                nanos: 0
            }
        );
    }

    #[test]
    fn test_chrono_timestamp_to_timestamp_proto() {
        let mut utc_date_time =
            DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(1350336377, 1234), Utc);
        let mut timestamp = chrono_timestamp_to_timestamp_proto(&utc_date_time);
        assert_eq!(
            timestamp,
            Timestamp {
                seconds: 1350336377,
                nanos: 1234
            }
        );
        utc_date_time =
            DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(1, 1234567890), Utc);
        timestamp = chrono_timestamp_to_timestamp_proto(&utc_date_time);
        assert_eq!(
            timestamp,
            Timestamp {
                seconds: 2,
                nanos: 234567890
            }
        );
    }
}
